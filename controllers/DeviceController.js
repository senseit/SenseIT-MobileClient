  
module.exports = {
  setDeviceName: function(ip, port, deviceId, newName){
    var link = 'http://'+ip+':'+port+'/devices/'+deviceId;
    var data = {
      deviceId: deviceId,
      name: newName,
    };

    fetch(link, {
      method: 'PUT',
      body: JSON.stringify(data), 
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.status !== 'Succesfully updated!'){
        console.warn("Error in device delete");
      }
    });
  },
  addNewRule: function(ip, port, deviceId, newRule){
    var link = 'http://'+ip+':'+port+'/devices/rules/'+deviceId;
    fetch(link, {
      method: 'POST',
      body: JSON.stringify(newRule), 
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.status !== 'Succesfully updated!'){
        console.warn("Error in new rule addition");
      }
    });
  },
  deleteRule: function(ip, port, deviceId, index){
    var link = 'http://'+ip+':'+port+'/devices/rules/'+deviceId;
    fetch(link, {
      method: 'DELETE',
      body: JSON.stringify({ruleIndex:index}), 
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.status !== 'Succesfully deleted!'){
        console.warn("Error in rule deletion");
      }
    });
  },
  updateRule: function(ip, port, deviceId, rule){
    var link = 'http://'+ip+':'+port+'/devices/rules/'+deviceId;
    fetch(link, {
      method: 'PUT',
      body: JSON.stringify(rule), 
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.status !== 'Succesfully updated!'){
        console.warn("Error in new rule update");
      }
    });
  },
  getRules: function(ip, port, deviceId, next){
    var link = 'http://'+ip+':'+port+'/devices/rules/'+deviceId;
    fetch(link, {
      method: 'Get',
    })
    .then((response) => response.json())
    .then((responseJson) => {
      next(responseJson);
    });
  }
}