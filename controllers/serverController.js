  
module.exports = {
    isServerOnline: function(ip, port, next, isUnMounted){
        const FETCH_TIMEOUT = 1000;
        let didTimeOut = false;

        new Promise(function(resolve, reject) {
            const timeout = setTimeout(function() {
                didTimeOut = true;
                reject(new Error('Request timed out'));
            }, FETCH_TIMEOUT);
            
            var link = 'http://'+ip+':'+port+'/server/';
            fetch(link)
            .then(function(response) {
                // Clear the timeout as cleanup
                clearTimeout(timeout);
                if(!didTimeOut) {
                    resolve(response);
                }
            })
            .catch(function(err) {
                console.warn('fetch failed! ', err);
                
                // Rejection already happened with setTimeout
                if(didTimeOut) return;
                // Reject with error
                reject(err);
            });
        }).then((response) => response.json())
        .then(function(response) {
            // Request success and no timeout
            if (!isUnMounted()){
                next(true, response);
            } 
        })
        .catch(function(err) {
            // Error: response error, request timeout or runtime error
            if (!isUnMounted()){
                next(false);
            }
        });
    },
    getDeviceList: function(ip, port, next){
        var link = 'http://'+ip+':'+port+'/devices/';

        fetch(link)
        .then((response) => response.json())
        .then((responseJson) => {
            next(responseJson);
        })
        .catch((error) => {
          console.warn(error);
        });
    },
    deleteDevice: function(ip, port, deviceId){
      var link = 'http://'+ip+':'+port+'/devices/'+deviceId;
      fetch(link, {
        method: 'delete'
      })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status !== 'Device deleted'){
          console.warn("Error in device delete");
        }
      });
    },
    deleteEvent: function(ip, port, eventId){

      var link = 'http://'+ip+':'+port+'/events/'+eventId;
      fetch(link, {
        method: 'delete'
      })
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status !== 'Event deleted'){
          console.warn("Error in event delete");
        }
      });
    }
}