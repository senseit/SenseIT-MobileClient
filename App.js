import React, {Component} from 'react';
import MainTab from './components/MainTab'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import ServerFinderTab from './components/ServerFinderTab';
import DeviceModal from './components/DeviceModal';
import NewRuleOverlay from './components/NewRuleOverlay';
import RNCalendarEvents from 'react-native-calendar-events'

import { Provider } from 'react-redux';
import configureStore from './store/ConfigureStore.js';
import TasksModal from './components/TasksModal';

const MainStack = createStackNavigator(
    {
      Home: {
        screen: MainTab,
      }
    },
    {
      initialRouteName: 'Home',
      defaultNavigationOptions: {
        headerStyle: {
          backgroundColor: '#4ab291',
        },
        headerTintColor: '#fff',
        title: "SenseIT",
        headerTitleStyle: { 
            textAlign:"center", 
            flex:1,
            fontSize: 25,
            marginTop: -2,
            color: '#fff',
            fontWeight: 'bold',
            letterSpacing: 4,
            fontFamily: 'fantasy',
        },
      },
    }
  );
  
  const RootStack = createStackNavigator(
    {
      Main: {
        screen: MainStack,
      },
      LoginModal: {
        screen: TasksModal,
      },
      ServerFinder:{
        screen: ServerFinderTab
      },
      DeviceModal:{
        screen: DeviceModal
      },
      NewRuleOverlay:{
        screen: NewRuleOverlay
      }
    },
    {
      headerMode: 'float ',
    }
  );
  
  const AppContainer = createAppContainer(RootStack);
  
  const store = configureStore({});

  export default class App extends React.Component {

    constructor() {
      super();
      this.state = {
        cal_auth: ''
      }
    }
    
    componentWillMount(){
      RNCalendarEvents.authorizationStatus()
      .then(status => {
        // if the status was previous accepted, set the authorized status to state
        this.setState({ cal_auth: status })
        if(status === 'undetermined') {
          // if we made it this far, we need to ask the user for access 
          RNCalendarEvents.authorizeEventStore()
          .then((out) => {
            if(out == 'authorized') {
              // set the new status to the auth state
              this.setState({ cal_auth: out })
            }
          })
         }
       })
     .catch(error => console.warn('Auth Error: ', error));
   
     // Android
     RNCalendarEvents.authorizeEventStore()
     .then((out) => {
       if(out == 'authorized') {
         // set the new status to the auth state
         this.setState({ cal_auth: out })
       }
     })
     .catch(error => console.warn('Auth Error: ', error));
      
    }
    render() {
      return (
        <Provider store={store}>
            <AppContainer />
        </Provider>
      );
    }
  }

