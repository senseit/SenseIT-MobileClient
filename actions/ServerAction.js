import { SERVER_CONNECTION, SERVER_DISCONNECTION } from "../constants/ActionTypes";

export function addServer(server) {
  return { type: SERVER_CONNECTION, server };
}

export function removeServer(server) {
    return { type: SERVER_DISCONNECTION, server };
}