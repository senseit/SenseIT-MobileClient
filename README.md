# SenseIT - Mobile Client

The mobile client is a cross-platform, React Native application with Redux~\cite{Redux} as a state management container. With the aid of the mobile application users have the possibility to create and manage online slave microcontrollers and their sensor rules.

## Getting started

Instructions for deploy the application to Android or IOS. The following commands install the mobile client to the connected device:

    npm install
    react-native link
    react-native run-android
  
## License

This project is licensed under the GPL License - see the [LICENSE.md](https://gitlab.com/senseit/SenseIT-MobileClient/blob/master/LICENSE.md) file for details