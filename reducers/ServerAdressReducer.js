import { SERVER_CONNECTION, SERVER_DISCONNECTION } from '../constants/ActionTypes';
import {AsyncStorage} from 'react-native';

const INITIAL_STATE = {
    serverIP: "",
    serverPort: "",
    serverName: ""
};

export const getServerSelector = (state) => ({ ...state.server });


const serverReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SERVER_CONNECTION: {
          
            AsyncStorage.setItem(action.server.serverIP, action.server.serverPort.toString())
            .catch((error) => { console.warn('Promise is rejected with error: ' + error) });

          return {
            serverIP : action.server.serverIP,
            serverPort : action.server.serverPort,
            serverName : action.server.serverName,
          };
        }
        case SERVER_DISCONNECTION:{
            return INITIAL_STATE;
        }
        default: {
          return state;
        }
      }
};

export default serverReducer;