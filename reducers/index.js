import { combineReducers } from 'redux';
import serverAddressReducer from './ServerAdressReducer';

// Root Reducer
const rootReducer = combineReducers({
  serverAdress: serverAddressReducer,
});

export default rootReducer;