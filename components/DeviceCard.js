import React, { Component } from 'react';
import {StyleSheet, Text, View, Animated} from 'react-native';
import { ListItem, Icon } from 'react-native-elements'


import { withNavigation } from 'react-navigation';

class DeviceCard extends Component {
    static navigationOptions = {
      title: "DeviceCard"
    }

    constructor(props) {
       super(props);
       this.state = {
          deviceName: props.deviceName,
          deviceId: props.deviceId,
          type:props.type,
          deleteFunction: props.deleteFunction,
        };

        this.setDeviceName = this.setDeviceName.bind(this);
       }

  setDeviceName(newName){
    this.setState({
      deviceName: newName
    });
  }


  _onLongPress = () => {
    this.state.deleteFunction(true, this.state.deviceId);
  }

  render() {
      const { navigate } = this.props.navigation;
      return (
      <View>
          <ListItem containerStyle={styles.buttonStyle}
            key={this.state.deviceId}
            title={<Text style={styles.textStyle}>{this.state.deviceName}</Text>}
            subtitle={"Type: " + this.state.type + "\n" + "ID: " + this.state.deviceId}
            rightIcon= {(<Icon name='settings'></Icon>)}
            onPress={() => navigate('DeviceModal', {deviceName: this.state.deviceName, deviceId: this.state.deviceId, refreshFunc:this.setDeviceName})}
            onLongPress = {this._onLongPress}
          />
      </View>
      );
  }
}

const styles = StyleSheet.create({
    textStyle:{
        fontSize:18,
        color:'#000'
    },
    buttonStyle: {
        marginTop: 1,
        backgroundColor: '#dbf0e9',
    },
});


export default withNavigation(DeviceCard);