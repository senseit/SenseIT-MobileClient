import React, { Component } from 'react';
import {StyleSheet, Text, View, Animated} from 'react-native';
import { Icon, Button } from 'react-native-elements'
import { connect } from 'react-redux';
import { getServerSelector } from '../reducers/ServerAdressReducer';
import { removeServer } from '../actions/ServerAction';

import DeviceListTab from './DeviceListTab'


class ConnectedServerCard extends Component {
    constructor(props) {
       super(props);
       this.state = {
          serverName: props.serverName,
          serverIP: props.serverIP,
          serverPort: props.serverPort,
          online: props.online,
          fadeAnim: new Animated.Value(0),
        };
        this.unmounted=false;
        this.changeOnline = this.changeOnline.bind(this);
        this.isUnMounted = this.isUnMounted.bind(this);
       }

    disableConnection = () => {
        this.props.dispatchServerConnection ({
            serverIP : this.state.serverIP,
            serverPort: this.state.serverPort,
            serverName: this.state.serverName
        });
        this.props.goBack();
    }

    changeOnline = (isOnline) =>{
        this.setState({online: isOnline});
    }

    isUnMounted = ()=>{
        return this.unmounted;
    }
    componentWillUnmount(){
        this.unmounted = true;
    }

    
  render() {
      return (
      <View> 
        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', margin:10, alignItems:'center'}}>
        <Text style={styles.textStyle}>
          Connected Server: {"\n"}
          {this.state.serverIP}{":"}{this.state.serverPort}
        </Text>
        <Button
          icon={<Icon name='link' color='#ffffff' />}
          title='Disconnect'
          buttonStyle={styles.buttonStyle} 
          onPress={this.disableConnection}/>
        </View>
        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', margin:10, alignItems:'center'}}>
          <Text style={styles.textStyle}> Available Devices</Text>
        </View>
        <DeviceListTab/>
      </View>  
      );
  }
}

const styles = StyleSheet.create({
    textStyle:{
        fontSize:18,
        color:'#000'
    },
    buttonStyle: {
        backgroundColor: '#4ab291',
    },
});


const mapStateToProps = (state) => getServerSelector(state);


function mapDispatchToProps (dispatch) {
return {
    dispatchServerConnection: (server) => dispatch(removeServer(server))
}
}
  
export default connect( mapStateToProps, mapDispatchToProps,)(ConnectedServerCard)