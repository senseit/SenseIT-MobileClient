import React, { Component } from 'react';
import {StyleSheet, Text, View, Picker, TextInput } from 'react-native';
import { Button } from 'react-native-elements'

export default class NewRuleOverlay extends Component {
  static navigationOptions = {
    title: 'New Rule',
  };
  constructor(props) {
    super(props);
    if (this.props.navigation.state.params.item === undefined){
      this.state = {
          inputSensorPort: "Digital 0",
          inputSensorType: "Trigger Sensor",
          inputSensorName: "",
          outputSensorPort: "Digital 1",
      };
    } else{
      this.state = {
        inputSensorPort: this.props.navigation.state.params.item.InputSensorPort,
        inputSensorType: this.props.navigation.state.params.item.InputSensorType,
        inputSensorName: this.props.navigation.state.params.item.InputSensorName,
        outputSensorPort: this.props.navigation.state.params.item.OutputSensorPort,
    };
    }
     this.addNewRuleFunc = this.props.navigation.state.params.addNewRuleFunc;
     this.setInputSensor = this.setInputSensor.bind(this);
     this.setOutputSensor = this.setOutputSensor.bind(this);
    }

  setOutputSensor(value){
    this.setState({
      outputSensorPort:value
    });
  }

  setInputSensor(value){
    this.setState({
      inputSensorPort:value
    });
  }
  getSensorPicker(title, stateValue, setStateFunc){
    return (
      <View style={styles.formContainer}>
            <Text style={{ fontSize: 18, fontWeight:'bold', color:'#4ab291'}}>{title}</Text>
            <Picker mode='dropdown'
                selectedValue={stateValue}
                style={{height: 50, width: 140, color:'#4ab291', marginLeft:10, marginRight:3}}
                onValueChange={(itemValue, itemIndex) =>
                  setStateFunc(itemValue)
                }>
                <Picker.Item label="Digital 0" value="Digital 0" />
                <Picker.Item label="Digital 1" value="Digital 1" />
                <Picker.Item label="Digital 2" value="Digital 2" />
                <Picker.Item label="Digital 3" value="Digital 3" />
                <Picker.Item label="Digital 4" value="Digital 4" />
                <Picker.Item label="Digital 5" value="Digital 5" />
                <Picker.Item label="Digital 6" value="Digital 6" />
                <Picker.Item label="Digital 7" value="Digital 7" />
            </Picker>
        </View>
    );
  }

  render() {
      var isTriggerSensor = this.state.inputSensorType == "Trigger Sensor";
      var sensorPicker = this.getSensorPicker("Input sensor port", this.state.inputSensorPort, this.setInputSensor);

      var triggerSensorPicker;
      if (isTriggerSensor) { 
        triggerSensorPicker = this.getSensorPicker("Output sensor port", this.state.outputSensorPort, this.setOutputSensor); 
      }

      var outputsensor;
      if (isTriggerSensor){
        outputsensor = this.state.outputSensorPort;
      } else {
        outputsensor = 'Digital 7';
      }

      var nameInput;
      if (this.props.navigation.state.params.item === undefined){
        nameInput = ( <TextInput  onChangeText={(text) => this.setState({inputSensorName: text})} 
                          style={{ fontSize: 18, color:'#4ab291', marginLeft:10, marginRight:3}}
                          placeholder={"Sensor Name"}>
                      </TextInput>)
      } else {
        nameInput = <TextInput  onChangeText={(text) => this.setState({inputSensorName: text})} 
                          style={{ fontSize: 18, color:'#4ab291', marginLeft:10, marginRight:3}}>
                          {this.props.navigation.state.params.item.InputSensorName}
                    </TextInput>
      }

      return (
        <View>
          <View style={styles.titleContainer}>
            <Text style={{ fontSize: 20, fontWeight:'bold', color:'#4ab291', marginTop:10 }}>Define new sensor rule</Text>
          </View>

          <View style={styles.formContainer}>
            <Text style={{ fontSize: 18, fontWeight:'bold', color:'#4ab291'}}>Input sensor name</Text>
            {nameInput}
          </View>

          {sensorPicker}
                
          <View style={styles.formContainer}>
              <Text style={{ fontSize: 18, fontWeight:'bold', color:'#4ab291'}}>Input sensor type</Text>
              <Picker mode='dropdown'
                  selectedValue={this.state.inputSensorType}
                  style={{height: 50, width: 170, color:'#4ab291', marginLeft:10, marginRight:3}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({inputSensorType: itemValue})
                  }>
                  <Picker.Item label="Trigger sensor" value="Trigger Sensor" />
                  <Picker.Item label="Task Reminder" value="Task Reminder"/>
              </Picker>
          </View>

          {triggerSensorPicker}

           <View style={styles.buttonContainer}>
            <Button title='Confirm' disabled={this.state.inputSensorName == ""} buttonStyle={styles.buttonStyle} onPress={() => {
                                                                              this.addNewRuleFunc(this.state.inputSensorPort, 
                                                                                                  this.state.inputSensorType,
                                                                                                  this.state.inputSensorName,
                                                                                                  this.state.outputSensorPort); 
                                                                              this.props.navigation.goBack()}
                                                                              }/>
            <Button title='Dismiss' buttonStyle={styles.buttonStyleRed} onPress={() => this.props.navigation.goBack()}/>
          </View>          
        </View>
      );
  }
}

const styles = StyleSheet.create({
    titleContainer: {
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 10
    },
    formContainer:{
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      backgroundColor: '#eff2f1',
      marginLeft: 5,
      paddingLeft: 5,
      marginRight: 5,
      borderBottomColor: 'black',
      borderBottomWidth: 1,
    },
    pickerStyle:{
      height: 50, 
      width: 140, 
      color:'#4ab291', 
      marginLeft:10, 
      marginRight:3
    },
    buttonContainer:{
      flexDirection: 'row',
      alignItems:'center',
      justifyContent:'center',
      marginTop:10,
    },
    buttonStyle: {
      marginTop: 10,
      backgroundColor: '#4ab291',
      paddingLeft: 10,
      paddingRight: 10,
      marginLeft: 10
    },
    buttonStyleRed:{
      marginTop: 10,
      backgroundColor: 'red',
      paddingLeft: 10,
      paddingRight: 10,
      marginLeft: 10
    }
});


