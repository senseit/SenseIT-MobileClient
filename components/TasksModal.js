import React, { Component } from 'react';
import {Text, View, ScrollView, StyleSheet, TextInput } from 'react-native';
import {AsyncStorage} from 'react-native';
import TaskCard from './TaskCard';
import { Icon, Overlay } from 'react-native-elements'
import NewTaskOverlay from './NewTaskOverlay';
import * as AddCalendarEvent from 'react-native-add-calendar-event';


export default class TasksModal extends Component {

    constructor(props) {
      super(props);
      this.state = {
        tasks: [],
        isDeleteOverlayVisible:false,
        newTaskMessage:''
      };

      this.updateTasks = this.updateTasks.bind(this);
      this.changeVisibility = this.changeVisibility.bind(this);
      this.updateDone = this.updateDone.bind(this);
      this.deleteFunc = this.deleteFunc.bind(this);
      this.addNewTask = this.addNewTask.bind(this);
      AsyncStorage.getItem('tasks')
      .then(req => JSON.parse(req))
      .then(this.updateTasks);
    }

    updateTasks(tasks) {
      this.setState({
        tasks: tasks
      });
    }

    updateDone(i, done){
      AsyncStorage.getItem('tasks')
      .then(req => JSON.parse(req))
      .then(function(tasks){
        tasks[i].done = done;
        AsyncStorage.setItem('tasks', JSON.stringify(tasks));
      });
    }

    deleteFunc(i){
      AsyncStorage.getItem('tasks')
      .then(req => JSON.parse(req))
      .then(function(tasks){
        tasks.splice(i,1);
        AsyncStorage.setItem('tasks', JSON.stringify(tasks));
      });
      tasks=this.state.tasks;
      tasks.splice(i, 1);
      this.setState({
        tasks:tasks
      })
    }

    changeVisibility(value){
      this.setState({
        isDeleteOverlayVisible:value
      });
    }

    addNewTask(message, addEvent){
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();

      today = mm + '/' + dd + '/' + yyyy;
      var newTask={
        done: false,
        message: message,
        date: today
      }
      AsyncStorage.getItem('tasks')
      .then(req => JSON.parse(req))
      .then(function(tasks){
        
        if (tasks === null){
          tasks = new Array();
        }
        tasks.push(newTask);
        AsyncStorage.setItem('tasks', JSON.stringify(tasks));
      })

      tasks=this.state.tasks;
      if (tasks === null){
        tasks = new Array();
      }
      tasks.push(newTask);
      this.setState({
        tasks:tasks
      })

      this.setState({
        isDeleteOverlayVisible:false
      });
      
      
      const eventConfig = {
        title :message,
        startDate: new Date()
        // and other options
      };

      if (addEvent){
        AddCalendarEvent.presentEventCreatingDialog(eventConfig)
        .catch((error) => {
          // handle error such as when user rejected permissions
          console.warn(error);
        });
      }
    }

    render() {
      taskCards = [];
  
      if (this.state.tasks !== null){
        for (let i=0; i< this.state.tasks.length; i++){
          taskCards.push (<TaskCard done={this.state.tasks[i].done} 
            message={this.state.tasks[i].message} 
            date={this.state.tasks[i].date} 
            key={this.state.tasks[i].message} 
            id={i}
            updateDone = {this.updateDone}
            deleteFunc = {this.deleteFunc}/>);
          
        }
      }
        
      var overlay =  (<NewTaskOverlay addFunc={this.addNewTask} hideFunc={()=>{this.changeVisibility(false)}}/>)

      return (
        <View>
        <ScrollView>
          
          <View style={styles.titleContainer}>
              <Text style={styles.titleStyle}>List of tasks</Text>
          </View>
          {
          taskCards
          }
          <Icon name="add" 
                reverse
                color='#4ab291'
                containerStyle={{flex:1, alignItems:'flex-end', margin:20}}
                style={styles.newButton} 
                onPress={()=>{this.changeVisibility(true)}}/>
         
        </ScrollView>
          {this.state.isDeleteOverlayVisible && overlay}
        </View>
      );
    }
  }

const styles = StyleSheet.create({
    textStyle:{
        fontSize:14,
        color:'#000'
    },
    titleStyle:{
      fontSize: 20, 
      fontWeight:'bold', 
      color:'#4ab291', 
      marginTop:10
    },
    buttonStyle: {
        marginTop: 1,
        backgroundColor: '#daf2da',
    },
    titleContainer: {
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      marginBottom: 10
    },
    newButton: {
      marginBottom: 10,
    }
});
