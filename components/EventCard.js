import React, { Component } from 'react';
import {StyleSheet, Text, View, Animated} from 'react-native';
import { ListItem, Icon } from 'react-native-elements'

class EventCard extends Component {

  constructor(props) {
      super(props);
      this.state = {
        source: props.sourceId,
        port: props.port,
        text: props.text,
        type: props.type,
        id:props.id,
        i:props.i,
        deleteFunc: props.deleteFunc,
        addNewTask: props.addNewTask,
      };
      }
  
  render() {
      return (
      <View>
          <ListItem containerStyle={styles.buttonStyle}
            key={this.state.id}
            title={
              <View>
                <Text style={styles.titleStyle}>{this.state.text}</Text>
                <Text style={styles.textStyle}>Source: {this.state.source}</Text>
                <Text style={styles.textStyle}>Port: {this.state.port}</Text>
              </View>
            }
            onPress={this.state.addNewTask}
            leftIcon= {(<Icon name='notifications'></Icon>)}
            rightIcon= {(<Icon name='delete' onPress={()=>this.state.deleteFunc(this.state.i)}></Icon>)}
          />
      </View>
      );
  }
}

const styles = StyleSheet.create({
    textStyle:{
        fontSize:14,
        color:'#000'
    },
    titleStyle:{
      fontSize:14,
      color:'#000',
      fontWeight: 'bold'
    },
    buttonStyle: {
        marginTop: 1,
        backgroundColor: '#daf2da',
    },
});


export default EventCard;