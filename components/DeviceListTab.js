import React, { Component } from 'react';
import { ScrollView, RefreshControl, StyleSheet, Text, View } from 'react-native';
import { Overlay, Icon, Button } from 'react-native-elements'
import { connect } from 'react-redux';
import DeviceCard from './DeviceCard';

var ServerController = require('../controllers/ServerController');

const mapStateToProps = state => {
    return { server: state.serverAdress };
};
  

class DeviceListTab extends Component {

    constructor(props) {
        super(props);
        this.state = {
          refreshing: false,
          online:false,
          updated:false,
          deviceList:[],
          isDeleteOverlayVisible:false,
        };
        this.toDeleteDeviceId="";
        this.updateDeviceList = this.updateDeviceList.bind(this);
        this.changeOverlayVisibility = this.changeOverlayVisibility.bind(this);

       
      }

    changeOverlayVisibility = (value, deviceId) =>{
      this.setState({isDeleteOverlayVisible:value});
      this.toDeleteDeviceId = deviceId;
    }

    _onDeletePress = () => {
      if (this.props.server.serverIP === '' || this.toDeleteDeviceId === '') return;
      ServerController.deleteDevice(this.props.server.serverIP, this.props.server.serverPort, this.toDeleteDeviceId);
      this.setState({updated:false, isDeleteOverlayVisible:false});
    }
    updateDeviceList = () => {
        if (this.props.server.serverIP === '') return;
        if (!this.state.updated){
            ServerController.getDeviceList (this.props.server.serverIP, this.props.server.serverPort, this.setdeviceListOnState);
        }
    }

    setdeviceListOnState = (list) =>{
        if (this.state.deviceList !== list){
            this.setState({deviceList: list, updated: true})
        }
    }

    _onRefresh = () => {
      this.setState({refreshing: true});
      this.setState({updated:false})
      this.updateDeviceList();
      this.setState({refreshing: false});
    }

    render() {
      this.updateDeviceList();
      var overlay =  (<Overlay  isVisible 
                                width ="auto" 
                                height="auto"
                                onBackdropPress={() => this.setState({ isDeleteOverlayVisible: false })}
                                >
                          <View style={styles.yesNoView}>        
                          <Text style={{fontSize:16, marginTop:5, fontWeight:"bold"}}>Do you want to delete the device?</Text>
                          </View>
                          <View style={styles.yesNoView}>
                          <Button title="Delete" buttonStyle={styles.buttonDeleteStyle} onPress={this._onDeletePress}/>
                          <Button title="Cancel"  buttonStyle={styles.buttonStyle} onPress={() => { this.changeOverlayVisibility(false)}}/>
                          </View>

                      </Overlay>)
      return (
        <View style={{flex: 1}}>
        <ScrollView  refreshControl={
                    <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                    />
                    }>  
        {
          this.state.deviceList.map((item, i) => (
            <DeviceCard deleteFunction={this.changeOverlayVisibility} deviceName={item.name} deviceId={item.deviceId} type={"Device"} key={i}/>    
        ))
        }
      </ScrollView >
      {this.state.isDeleteOverlayVisible && overlay}
             
      </View>
      );
    }
  }

const styles = StyleSheet.create({
containerStyle: {
    backgroundColor: '#fcfce3',
    justifyContent: 'space-around',
    marginBottom: 2,
},
bottom: {
    fontSize: 20,
    textAlign: 'center',
    marginTop: 15,
    color: 'grey',
    fontStyle: 'italic',
  },
yesNoView:{
  flexDirection: 'row',
  justifyContent: 'center'
},
buttonStyle:{
  width:90,
  height:35,
  marginTop:40,
  marginRight:10,
  backgroundColor:"#4ab291"
},
buttonDeleteStyle:{
  width:90,
  height:35,
  marginTop:40,
  marginRight:10,
  backgroundColor:"#ff0000"
}

});


export default connect( mapStateToProps )(DeviceListTab)