import React, { Component } from 'react';
import {StyleSheet, Text, View} from 'react-native';
import { ListItem, Icon, CheckBox } from 'react-native-elements'


class TaskCard extends Component {

  constructor(props) {
      super(props);
      this.state = {
        id:       props.id,
        done:     props.done,
        message:  props.message,
        date:     props.date
      };

      this.deleteFunc = props.deleteFunc;
      this.updateDone = props.updateDone;
      }

  getStyle (){
    if (this.state.done){
      return styles.doneTitleStyle;
    }

    return styles.titleStyle;
  }
  render() {
      return (
          <ListItem
            key={this.state.id}
            title={
              <View>
                
                <CheckBox
                  title={
                    <View>
                      <Text style={this.getStyle()}>{this.state.message}</Text>
                      <Text style={styles.textStyle}>Created on: {this.state.date}</Text>
                    </View>
                  }
                  containerStyle={{margin:-15, backgroundColor:'white'}}
                  checked={this.state.done}
                  checkedColor = {'#4ab291'}
                  onLongPress = {()=> {
                    this.deleteFunc(this.state.id);
                  }}
                  onPress={() => {
                    this.updateDone(this.state.id, !this.state.done);
                    this.setState({
                      done: !this.state.done
                    });
                  }} />
                </View>
            }
          />
          

      );
  }
}

const styles = StyleSheet.create({
    textStyle:{
        fontSize:10,
        color:'#000'
    },
    titleStyle:{
      fontSize:14,
      color:'#000',
      fontWeight: 'bold'
    },
    doneTitleStyle:{
      fontSize:14,
      color:'grey',
      fontWeight: 'bold',
      textDecorationLine: 'line-through', 
      textDecorationStyle: 'solid'
    },
    itemStyle:{
      flexDirection:'row',
    }
});


export default TaskCard;