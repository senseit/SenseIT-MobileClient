import React, { Component } from 'react';
import { View, TextInput, Text } from 'react-native';
import { Overlay, CheckBox } from 'react-native-elements'
import * as AddCalendarEvent from 'react-native-add-calendar-event';

class NewTaskOverlay extends Component {

  constructor(props) {
      super(props);
      this.state = {
        newTaskMessage: '',
        addevent:false
      };

      this.addFunc = props.addFunc;
      this.hideFunc = props.hideFunc;
      }

  render() {
      return (
              <Overlay  isVisible 
                  height="auto"
                  width="auto"
                  containerStyle={{flex:1, alignItems:'flex-start', flexDirection:'row', justifyContent:'center',
                                    paddingTop:100}}
                   onBackdropPress={this.hideFunc}
                  >
              <View style={{alignItems:'center', justifyContent:'center'}}>
                <Text style={{fontSize:15, fontWeight:'bold'}}> Insert New Task</Text>
                <TextInput  onChangeText={(text) => this.setState({newTaskMessage: text})} 
                  style={{ fontSize: 18, color:'#4ab291'}}
                  onBlur={() => {this.addFunc(this.state.newTaskMessage, this.state.addevent);}}
                  placeholder='New task description'>
                </TextInput>
                <CheckBox
                  title={ <Text>Create event to calendar</Text> }
                  checked={this.state.addevent}
                  checkedColor = {'#4ab291'}
                  onPress={() => {
                    this.setState({
                      addevent: !this.state.addevent
                    });
                  }} />
              </View>
          </Overlay>
      );
  }
}


export default NewTaskOverlay;