import React, { Component } from 'react';
import {StyleSheet, Text, View, Animated} from 'react-native';
import { Icon, Card, Button} from 'react-native-elements'
import { connect } from 'react-redux';
import { getServerSelector } from '../reducers/ServerAdressReducer';
import { addServer } from '../actions/ServerAction';

var ServerController = require('../controllers/ServerController');

class ServerCard extends Component {
    constructor(props) {
       super(props);
       this.state = {
          serverName: props.serverName,
          serverIP: props.serverIP,
          serverPort: props.serverPort,
          online: props.online,
          fadeAnim: new Animated.Value(0),
        };
        this.unmounted=false;
        this.changeOnline = this.changeOnline.bind(this);
        this.isUnMounted = this.isUnMounted.bind(this);
       }

    createConnecton = () => {
        this.props.dispatchServerConnection ({
            serverIP : this.state.serverIP,
            serverPort: this.state.serverPort,
            serverName: this.state.serverName
        });
        this.props.goBack();
    }

    changeOnline = (isOnline) =>{
        this.setState({online: isOnline});
    }

    isUnMounted = ()=>{
        return this.unmounted;
    }
    componentWillUnmount(){
        this.unmounted = true;
    }

    cycleAnimation() {
        ServerController.isServerOnline(this.state.serverIP, this.state.serverPort, this.changeOnline, this.isUnMounted);

        Animated.sequence([
          Animated.timing(this.state.fadeAnim, {
            toValue: 1,
            duration: 1000,
            delay: 1100
          }),
          Animated.timing(this.state.fadeAnim, {
            toValue: 0,
            duration: 1000
          })
        ]).start(() => {
          if (!this.unmounted){  
            this.cycleAnimation();
          } 
        });
      }

    componentDidMount() {
        this.cycleAnimation();
      }
    
    getBlinkingled(){
        if (this.state.online){
            return ( <Animated.Image style={{opacity:this.state.fadeAnim, width:30, height:30}}
                source={require('../images/green-circle.jpg')}/> )
        } 
        return <Text>Offline</Text>;
    }
    
  render() {
      return (<Card
        title={this.state.serverName}>
        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
        <Text style={styles.textStyle}>
          Address: {this.state.serverIP} {"\n"}
          Port: {this.state.serverPort}
        </Text>
        { this.getBlinkingled() }
        </View>
        <Button
          icon={<Icon name='link' color='#ffffff' />}
          title='Connect'
          buttonStyle={styles.buttonStyle} 
          disabled={!this.state.online}
          onPress={this.createConnecton}/>
      </Card>
      );
  }
}

const styles = StyleSheet.create({
    textStyle:{
        fontSize:18,
        color:'#000'
    },
    buttonStyle: {
        marginTop: 10,
        backgroundColor: '#4ab291',
    },
});


const mapStateToProps = (state) => getServerSelector(state);


function mapDispatchToProps (dispatch) {
return {
    dispatchServerConnection: (server) => dispatch(addServer(server))
}
}
  
export default connect( mapStateToProps, mapDispatchToProps,)(ServerCard)