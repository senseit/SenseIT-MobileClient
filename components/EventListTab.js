import React, { Component } from 'react';
import { ScrollView, Text, StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';

import EventCard from './EventCard';
var ServerController = require('../controllers/ServerController');
import NewTaskOverlay from './NewTaskOverlay';
import {AsyncStorage} from 'react-native';
import * as AddCalendarEvent from 'react-native-add-calendar-event';


const mapStateToProps = state => {
  return { server: state.serverAdress };
};

class EventListTab extends Component {

  constructor(props) {
    super(props);
    this.state = {
      online: true,
      events: [],
      newTaskOverLay: false,
    };
    this.unmounted=false;
    this.changeOnline = this.changeOnline.bind(this);
    this.cycleRequest = this.cycleRequest.bind(this);
    this.isUnMounted = this.isUnMounted.bind(this);
    this.deleteEvent = this.deleteEvent.bind(this);
    this.addNewTask = this.addNewTask.bind(this);
    this.changeVisibility = this.changeVisibility.bind(this);
  }

  componentWillUnmount(){
    this.unmounted = true;
  }
  
  changeVisibility(value){
    this.setState({
      newTaskOverLay: value
    })
  }

  addNewTask(message, addEvent){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    today = mm + '/' + dd + '/' + yyyy;
    var newTask={
      done: false,
      message: message,
      date: today
    }
    AsyncStorage.getItem('tasks')
    .then(req => JSON.parse(req))
    .then(function(tasks){
      
      if (tasks === null){
        tasks = new Array();
      }
      tasks.push(newTask);
      AsyncStorage.setItem('tasks', JSON.stringify(tasks));
    })

    this.changeVisibility(false);
    
    const eventConfig = {
      title :message,
      startDate: new Date()
      // and other options
    };

    if (addEvent){
      AddCalendarEvent.presentEventCreatingDialog(eventConfig)
      .catch((error) => {
        // handle error such as when user rejected permissions
        console.warn(error);
      });
    }
  }

  deleteEvent(index){
    var online = this.props.server.serverIP !== "";
    if (!online){
      return; 
    }
    var events = this.state.events;;
    var id = events[index].id;
    ServerController.deleteEvent(this.props.server.serverIP, this.props.server.serverPort, id);
    this.setState({
      events:[]
    })
    this.cycleRequest();
  }

  changeOnline = (isOnline, event) =>{
    this.setState({online: isOnline, events: event});
  }

  isUnMounted = ()=>{
    return this.unmounted;
  }

  cycleRequest() {
    var online = this.props.server.serverIP !== "";
    if (!online){
      return;
    }
    ServerController.isServerOnline(this.props.server.serverIP, this.props.server.serverPort, this.changeOnline, this.isUnMounted);
  }

  componentDidMount() {
      setInterval( this.cycleRequest, 1500);
    }

  render() {
    var online = this.props.server.serverIP !== "";
    if (!online || !this.state.online){
        return (
          <Text style={styles.bottom}>No server is connected!</Text>
        )
    } 
    var inside;
    if (this.state.events !== undefined){
      inside = (
        this.state.events.map((item, i) => (
          <EventCard key={i}
                    i = {i}
                    id={item.id} 
                    text={item.text}
                    port={item.port}
                    type={item.type}
                    sourceId={item.sourceId}
                    deleteFunc={this.deleteEvent}
                    addNewTask={()=>{this.changeVisibility(true)}}/>
        ))
      )
    }
    var overlay =  (<NewTaskOverlay addFunc={this.addNewTask} hideFunc={()=>{this.changeVisibility(false)}}/>)
    return (
      <View>
        <ScrollView> 
            <Text style={styles.title}>System Events</Text>
            {
            inside
            }
        </ScrollView >
        {this.state.newTaskOverLay && overlay}
      </View>
    );
  }
}


const styles = StyleSheet.create({
  bottom: {
      fontSize: 20,
      textAlign: 'center',
      marginTop: 15,
      color: 'grey',
      fontStyle: 'italic',
    },
  title: {
    fontSize: 20,
    textAlign: 'center',
    marginTop: 15,
    marginBottom: 15,
    color: 'black',
  },
});

export default connect( mapStateToProps )(EventListTab)