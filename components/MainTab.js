import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Icon, Button } from 'react-native-elements'
import EventListTab from './EventListTab';
import NewRuleOverlay from './NewRuleOverlay';
import TasksModal from './TasksModal';

export default class MainTab extends Component {
    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};
        return {
           headerLeft: (
            <Button
              onPress={() => navigation.navigate('LoginModal')}
              color="#fff"
              buttonStyle={styles.buttonStyle}
              icon={<Icon name='menu' color='#ffffff' style={{width:39, height:39}}/>}
              title=""
            />
          ),
          headerRight: (
            <Button
            onPress={() => navigation.navigate('ServerFinder')}
            color="#fff"
            buttonStyle={styles.buttonStyle}
            icon={<Icon name='laptop' color='#ffffff' style={{width:39, height:39}}/>}
            title=""
          />
          )
        };
      };

  render() {
    return (
        <View style={styles.container}>
          <EventListTab/>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#ffffff',
    },
    buttonStyle: {
      backgroundColor: '#4ab291',
      marginLeft: 10,
      marginRight: 10,
      width: 40,
      height: 40,
    }
  });
  