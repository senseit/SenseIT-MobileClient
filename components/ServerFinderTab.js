import React, { Component } from 'react';
import {StyleSheet, Text, View, ScrollView, RefreshControl} from 'react-native';
import { Icon } from 'react-native-elements'
import Zeroconf from 'react-native-zeroconf'
import ServerCard from './ServerCard'
import { connect } from 'react-redux';
import ConnectedServerCard from './ConnectedServerCard';
import {AsyncStorage} from 'react-native';

const mapStateToProps = state => {
    return { server: state.serverAdress };
};

class ServerFinderTab extends Component {

  constructor(props) {
      super(props);
      
      this.initZeroConf();

      this.state = {
        refreshing: false,
        serverName: "",
        serverIP:"",
        serverPort:0,
        online:false,
        prevServerPort:0,
        prevServerIP:"",
      };

      this.unmount=false;
      this.onRefresh = this.onRefresh.bind(this);
      this.updatePrevServer = this.updatePrevServer.bind(this);
  }

  componentDidMount() {
    this.loadData(this.updatePrevServer);
  }

  loadData(updateFunction){
    AsyncStorage.getAllKeys().then(function(strings){
      if (strings[0] === 'tasks')
        return;
        
      if (strings.length >= 1){
        AsyncStorage.getItem(strings[0]).then(function(result){
          updateFunction(result, strings[0]);
        }).catch((error) => { console.warn('Promise is rejected with error: ' + error) });
      }
    }).catch((error) => { console.warn('Promise is rejected with error: ' + error) });
  }

  initZeroConf (){
    this.zeroConf = new Zeroconf();
    this.zeroConf.on('resolved', service => {
      if (!this.unmount){
        if (service.txt.ip !== undefined){
          var IP = service.txt.ip.replace(/-/g, '.');
          this.setState({refreshing: false});
          this.setState({ serverName: service.name,
                          serverIP: IP,
                          serverPort: service.port,
                          online: true});
          this.zeroConf.stop();  
        }
      }
    });
    
    this.zeroConf.on('error', err => {
        console.warn('[Error on ZeroConf]', err);
    });
  }

  updatePrevServer = (port, ip) => {
    this.setState({prevServerPort: port, prevServerIP: ip});
  }

  componentWillUnmount(){
      this.unmount = true;
      this.zeroConf.stop();
  }

  onRefresh = () => {
      if (!this.unmount){
          this.setState({refreshing: true});
          this.setState({
              online: false
          });
          this.zeroConf.scan();
          var that = this;
          setTimeout(function(){
              if (!that.unmount){ 
                  that.setState({refreshing: false});
              }},2000);
      }
  }

  render() {
  var insideScrollView;
  if (this.props.server.serverIP !== ""){
      insideScrollView = <ConnectedServerCard  serverIP={this.props.server.serverIP}
                                      serverName={this.props.server.serverName}
                                      serverPort={this.props.server.serverPort}
                                      online={this.state.online}
                                      goBack={this.props.navigation.goBack}
      />
  } else {
      if (this.state.online){
          insideScrollView = <ServerCard  serverIP={this.state.serverIP}
                                          serverName={this.state.serverName}
                                          serverPort={this.state.serverPort}
                                          online={this.state.online}
                                          goBack={this.props.navigation.goBack}
                                          />
      } else{

          var prevServerCard;
          if (this.state.prevServerIP !=="" && this.state.prevServerPort !==0){
            prevServerCard = (<ServerCard   serverIP={this.state.prevServerIP}
                                            serverName='Previous Connected Server'
                                            serverPort={this.state.prevServerPort}
                                            online={this.state.online}
                                            goBack={this.props.navigation.goBack}
                                                              />);
          }
          insideScrollView = (
          <View>
          {prevServerCard}
          <Text style={styles.bottom}>You are not connected to the SenseIT Server!{"\n"}Pull down to refresh...</Text>              
          <Icon name='phonelink-off' 
              color='#ededde'
              size={200}></Icon>
          </View>
          );
      }
  }
  return (
  <ScrollView containerStyle={styles.container}  
              refreshControl={
              <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
              />
              }> 
  {insideScrollView}
  </ScrollView >

  )}
}


const styles = StyleSheet.create({
    bottom: {
        fontSize: 15,
        textAlign: 'center',
        marginTop: 15,
        color: 'grey',
        fontWeight: 'bold',
      },
    container: {
      flex: 2,
    }
});

export default connect( mapStateToProps )(ServerFinderTab)