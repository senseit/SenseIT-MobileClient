import React, { Component } from 'react';
import {Text, ScrollView, Image, View, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import { Button, Icon } from 'react-native-elements'
import { connect } from 'react-redux';


var DeviceController = require('../controllers/DeviceController');

const mapStateToProps = state => {
  return { server: state.serverAdress };
};

class DeviceModal extends Component {
  static navigationOptions = {
    title: 'Device Modal',
  };
    constructor(props) {
      super(props);
      this.state = {
        deviceName: this.props.navigation.state.params.deviceName,
        deviceId: this.props.navigation.state.params.deviceId,
        sensorRules:[]
      };

      this.lastEdited = 0;
      this.addNewRule = this.addNewRule.bind(this);
      this.editRule = this.editRule.bind(this);
      this.removeRule = this.removeRule.bind(this);
      this._onSubmitEditings = this._onSubmitEditings.bind(this);
      this.setRules = this.setRules.bind(this);
      DeviceController.getRules(this.props.server.serverIP, this.props.server.serverPort, this.state.deviceId, this.setRules);
    }

    _onSubmitEditings(){
      DeviceController.setdeviceName(this.props.server.serverIP, this.props.server.serverPort, this.state.deviceId, this.state.deviceName);
      this.props.navigation.state.params.refreshFunc(this.state.deviceName);
    }

    setRules = (rules) =>{
      this.setState({
        sensorRules: rules.rules
      });
    }
 
    addNewRule(sensorPort, sensorType, sensorName, outputSensorPort){
      var newRule = {
        InputSensorName:sensorName,
        InputSensorPort:sensorPort,
        InputSensorType:sensorType,
        OutputSensorPort:outputSensorPort,
      };
      this.setState({ sensorRules: [...this.state.sensorRules, newRule] });
      DeviceController.addNewRule(this.props.server.serverIP, this.props.server.serverPort, this.state.deviceId, newRule);
    }

    editRule(sensorPort, sensorType, sensorName, outputSensorPort){
     
      var newRule = {
        InputSensorName:sensorName,
        InputSensorPort:sensorPort,
        InputSensorType:sensorType,
        OutputSensorPort:outputSensorPort,
      };
      var rules = this.state.sensorRules;
      rules[this.lastEdited] = newRule;
      this.setState({ sensorRules: rules });

      var ruleWithIndex ={
          ruleIndex : this.lastEdited,
          InputSensorName:sensorName,
          InputSensorPort:sensorPort,
          InputSensorType:sensorType,
          OutputSensorPort:outputSensorPort,
      }
      console.log('udpate elott');
      DeviceController.updateRule(this.props.server.serverIP, this.props.server.serverPort, this.state.deviceId, ruleWithIndex );
    }

    removeRule(i){
      var rules = this.state.sensorRules;
      rules.splice(i, 1);
      this.setState({ sensorRules: rules });
      DeviceController.deleteRule(this.props.server.serverIP, this.props.server.serverPort, this.state.deviceId, this.lastEdited );
    }

    render() {
      return (
        <ScrollView style={{backgroundColor: 'white'}}>
          <View style={styles.titleContainer}>
            <Text style={{ fontSize: 20, fontWeight:'bold', color:'#4ab291', marginTop:10 }}>Device Configuration</Text>
          </View>

          <View style={styles.formContainer}>
            <Text style={{ fontSize: 18, fontWeight:'bold', color:'#4ab291'}}>Name:</Text>
            <TextInput  onChangeText={(text) => this.setState({deviceName: text})} 
                        style={{ fontSize: 18, color:'#4ab291'}}
                        onBlur={this._onSubmitEditings}>
                            {this.state.deviceName}
            </TextInput>
          </View>
       
          <View style={styles.formContainer}>
            <Text style={{ fontSize: 18, fontWeight:'bold', color:'#4ab291'}}>ID:</Text>
            <TextInput editable={false} selectTextOnFocus={false} style={{ fontSize: 18, color:'#4ab291'}}>{this.state.deviceId}</TextInput>
          </View>

          {
          this.state.sensorRules.map((item, i) => (
            <TouchableOpacity  style={styles.formContainer} key={i} onPress={()=> {
                this.lastEdited = i;
                this.props.navigation.navigate('NewRuleOverlay', {addNewRuleFunc:this.editRule, item: item})
              }} 
              onLongPress={
                ()=>{
                  this.removeRule(i);
                }
              }>
              <View style={{flexDirection:'column'}}>
                <Text style={{ fontSize: 18, fontWeight:'bold', color:'#4ab291'}}>Defined rule:</Text>
                <Text style={{ fontSize: 18, fontWeight:'bold', color:'#4ab291'}}>Sensor Name: {item.InputSensorName}</Text>
                
                <Text style={{ fontSize: 18, color:'#4ab291'}}>{item.InputSensorPort} triggers {item.OutputSensorPort} </Text>
                
              </View>
              
            </TouchableOpacity >
          ))
          }

          <View style={styles.titleContainer}>
          <Button
            icon={<Icon name='add' color='#ffffff' />}
            title='Add new rule'
            buttonStyle={styles.buttonStyle} 
            onPress={() => this.props.navigation.navigate('NewRuleOverlay', {addNewRuleFunc:this.addNewRule})}
            />
          </View>
        </ScrollView>
      );
    }
  }

const styles = StyleSheet.create({
  
  titleContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10
  },
  formContainer:{
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eff2f1',
    marginLeft: 5,
    paddingLeft: 5,
    marginRight: 5,
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  buttonStyle: {
    marginTop: 10,
    backgroundColor: '#4ab291',
    paddingLeft: 10,
    paddingRight: 10,
  },
  });

  export default connect( mapStateToProps )(DeviceModal)